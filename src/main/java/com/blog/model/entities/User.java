package com.blog.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idUser;
    private String name;
    @Column(unique = true)
    @NotNull
    @Email(regexp = ".*@.*\\..*", message = "email must contain @ and . symbols")
    private String email;
    @NotNull
    @Size(min = 8, message = "password must be minimum, 8 characters long")
    private String password;
    private String description;
    private String profilePicture;
    @OneToMany(mappedBy = "articleUser", cascade = CascadeType.REMOVE)
    private Collection<Article> articleList;
    @OneToMany(mappedBy = "commentUser", cascade = CascadeType.REMOVE)
    private Collection<Comment> commentList;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(
                    name = "id_user", referencedColumnName = "idUser"),
            inverseJoinColumns = @JoinColumn(
                    name = "id_role", referencedColumnName = "idRole"))
    private Collection<Role> roles;

    public User(String name, String email, String password, String description, String profilePicture,
                Collection<Article> articleList, Collection<Comment> commentList, Collection<Role> roles) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.description = description;
        this.profilePicture = profilePicture;
        this.articleList = articleList;
        this.commentList = commentList;
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return idUser == user.idUser &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(description, user.description) &&
                Objects.equals(profilePicture, user.profilePicture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, name, email, password, description, profilePicture);
    }

    public Collection<Role> getRoles() {
        if (roles == null) {
            return new ArrayList<>();
        }
        return roles;
    }
}
