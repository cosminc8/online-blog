package com.blog.model.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idArticle;
    @Column(unique = true)
    private String title;
    private String content;
    @ManyToOne
    @JoinColumn(name = "idUser")
    private User articleUser;
    @OneToMany(mappedBy = "commentArticle", cascade = CascadeType.REMOVE)
    private Collection<Comment> commentList;

    public Article(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return idArticle == article.idArticle &&
                Objects.equals(title, article.title) &&
                Objects.equals(content, article.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idArticle, title, content);
    }
}
