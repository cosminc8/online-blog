package com.blog.controllers;

import com.blog.model.entities.Article;
import com.blog.model.repositories.ArticleRepository;
import com.blog.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/allarticles")
    public String showAllArticles(Model model){
        model.addAttribute("articles", articleRepository.findAll());
        return "allArticles";
    }

    @PostMapping("/newarticle")
    public String saveArticle(Article article, Model model){
        article.setArticleUser(userRepository.getById(1L));
        articleRepository.save(article);
        model.addAttribute("articles", articleRepository.findAll());
        return "allArticles";
    }

    @GetMapping("/newarticle")
    public String showArticleForm(Model model){
        model.addAttribute("article", new Article());
        return "articleForm";
    }
}
