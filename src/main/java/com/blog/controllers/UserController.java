package com.blog.controllers;

import com.blog.model.entities.Role;
import com.blog.model.entities.User;
import com.blog.model.repositories.RoleRepository;
import com.blog.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Collection;

@Controller
public class UserController {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/allusers")
    public String showAllUsers(Model model){
        model.addAttribute("users", userRepository.findAll());
        return "allUsers";
    }

    @GetMapping("/newuser")
    public String showUserForm(Model model){
        model.addAttribute("user", new User());
        return "userForm";
    }

    @PostMapping("/newuser")
    public String saveUser(@Valid @ModelAttribute("user") User user, Errors errors, Model model){
        if (errors.hasErrors()){
            return "userForm";
        }
        prepareUserForRegistration(user);
        model.addAttribute("users", userRepository.findAll());
        return "redirect:allarticles";
    }

    private void prepareUserForRegistration(User user){
        String passwordEncoded = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(passwordEncoded);
        Role default_role = roleRepository.getByIdRole(1l);
        Collection<Role> newRoles = user.getRoles();
        newRoles.add(default_role);
        user.setRoles(newRoles);
        userRepository.save(user);
    }
}
