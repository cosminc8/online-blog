package com.blog.controllers;

import com.blog.model.entities.Comment;
import com.blog.model.repositories.ArticleRepository;
import com.blog.model.repositories.CommentRepository;
import com.blog.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CommentController {
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/allcomments")
    public String showAllAuthors(Model model){
        model.addAttribute("comments", commentRepository.findAll());
        return "allComments";
    }

    @GetMapping("/newcomment")
    public String showAuthorForm(Model model){
        model.addAttribute("comment", new Comment());
        return "commentForm";
    }

    @PostMapping("/newcomment")
    public String saveAuthor(Comment comment, Model model){
        comment.setCommentUser(userRepository.getById(1l));
        comment.setCommentArticle(articleRepository.getById(4l));
        commentRepository.save(comment);
        model.addAttribute("comments", commentRepository.findAll());
        return "allComments";
    }
}
